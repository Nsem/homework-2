//
//  SPIStar.m
//  Lection2
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

- (instancetype)initWithName:(NSString *)name {
	self = [super initWithType:SPISpaceObjectTypeStar name:name];
	if (self) {
		
	}
	return self;
}

- (NSNumber*)Weight {
	return _weight;
}

+ (NSString *)typeStringForType:(SPIStarType)starType {
	switch (starType) {
		case SPIStarTypeBig:
			return @"Big Star";
		
		case SPIStarTypeSmall:
			return @"Small Star";
			
		default:
			return @"Unknown Star";
	}
}

- (instancetype)initWithType:(SPIStarType)starType starName:(NSString *)starName {
	self = [super init];
	if (self) {
		_starType = starType;
		_starName = starName;
	}
	return self;
}

- (NSString *)title {
	return [NSString stringWithFormat:@"%@ %@", [SPIStar typeStringForType:self.starType], self.starName];
}

- (NSString *)description {
	return [NSString stringWithFormat:@"\n Star: %@\n Weight: %@\n Type: %@", self.starName, self.weight, [SPIStar typeStringForType:self.starType]];
}

@end
