//
//  StarSystem.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 05/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStarSystem.h"

@implementation SPIStarSystem

- (instancetype)init {
    return [self initWithName:@"Random Star System" age:@(arc4random())];
}

- (instancetype)initWithName:(NSString *)name age:(NSNumber *)age {
    self = [super init];
    if (self) {
        _name = name;
        _age = age;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nStar system: %@, age: %@", self.name, self.age];
}

@end
