//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIStar.h"

int main(int argc, const char * argv[]) {
	
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
	SPIStarSystem *betaStarSystem = [[SPIStarSystem alloc] initWithName:@"Procyon" age:@(350000)];
	SPIStarSystem *gammaStarSystem = [[SPIStarSystem alloc] initWithName:@"Mira" age:@(45700000000001)];
	SPIStarSystem *zetaStarSystem = [[SPIStarSystem alloc] initWithName:@"Delta Cephei" age:@(999836712)];
	
	NSArray *nameSystem = @[alphaStarSystem.name, betaStarSystem.name, gammaStarSystem.name, zetaStarSystem.name];
	NSArray *systems = @[alphaStarSystem, betaStarSystem, gammaStarSystem, zetaStarSystem];
	
	
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
	
	SPIAsteroidField *zotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Zota"];
	zotaAsteroidField.density = 120000;
	
	SPIAsteroidField *motaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Mota"];
	motaAsteroidField.density = 800000321;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
	
	SPIPlanet *coruscantPlanet = [[SPIPlanet alloc] initWithName:@"Coruscant"];
	coruscantPlanet.atmosphere = YES;
	coruscantPlanet.peoplesCount = 2341234122;
	
	//create stars
	SPIStar *sunStar = [[SPIStar alloc] initWithType:SPIStarTypeBig starName:@"Sun" ];
	sunStar.weight = @(1000000);
	
	SPIStar *altairStar = [[SPIStar alloc] initWithType:SPIStarTypeDefault starName:@"Altair"];
	altairStar.weight = @(100000);
	
	SPIStar *maulStar = [[SPIStar alloc] initWithType:SPIStarTypeSmall starName:@"Maul"];
	maulStar.weight = @(56122);
	
    
    alphaStarSystem.spaceObjects = @[vulkanPlanet, hotaAsteroidField, sunStar, plutoPlanet];
	betaStarSystem.spaceObjects = @[gallifreiPlanet, nabooPlanet, zotaAsteroidField, maulStar];
	gammaStarSystem.spaceObjects = @[altairStar, coruscantPlanet, motaAsteroidField];
	zetaStarSystem.spaceObjects = @[];
	
	
	
	
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    [spaceship loadStarSystem:systems systemsName:nameSystem];
	
	
	
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
	[gameObjects addObjectsFromArray:betaStarSystem.spaceObjects];
	[gameObjects addObjectsFromArray:gammaStarSystem.spaceObjects];
	[gameObjects addObjectsFromArray:zetaStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];

	
    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}


