//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

bool choise = YES;

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(NSArray *)starSystems systemsName:(NSArray *)systemsName {
    _starSystems = starSystems;
	_starSystem = starSystems.firstObject;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
	_systemsName = systemsName;
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];
	
	if ([self availableCommands] == nil) {
		NSLog(@"\n This star system is empty\n");
		_nextCommand = 1;
	}
	else {
		printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
		printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
		fgets(command, 255, stdin);
		int commandNumber = atoi(command);
		self.nextCommand = commandNumber;
	}
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
	
	if (choise == NO) { //choose another system
		_starSystem = [_starSystems objectAtIndex:_nextCommand - 1];
		choise = YES;
		_nextCommand = 0;
		if ([_starSystem.spaceObjects count] == 0) {
			_starSystem = nil;
		}
		_currentSpaceObject = _starSystem.spaceObjects.firstObject;
	}
	
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
			else if (currentSpaceObjectIndex == 0)
				choise = NO;
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
			else if (currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1))
				choise = NO;
            break;
        }
		
		case SPIPlayerSpaceshipCommandFlyToNextStarSystem: {
			printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
			break;
		}
	
        default:
            break;
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
	if (choise == NO){ //star systems list
		NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
		NSInteger command = SPIPlayerSpaceshipCommandFlyToNextStarSystem;
		NSString *description = [self descriptionForCommand:command];
		NSInteger systemIndex = [self.systemsName indexOfObject: self.systemsName.firstObject];
		
		for (NSInteger i = systemIndex; i < [_systemsName count]; i++) {
			if (description) {
				NSString *systemsName = [self.systemsName objectAtIndex: i];
				[mutableDescriptions addObject: [NSString stringWithFormat:@"%ld. Fly to %@ star system", (long)i + 1, systemsName]];
			}
		}
		return [mutableDescriptions componentsJoinedByString:@"\n"];
	}
	
    else if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandFlyToNextObject; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
		
		case SPIPlayerSpaceshipCommandFlyToNextStarSystem: {
			commandDescription = [NSString stringWithFormat:@""];
			break;
		}
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@""];
            break;
        }
            
        default:
            break;
    }
    return commandDescription;
}


@end
