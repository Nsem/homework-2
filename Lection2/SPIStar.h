//
//  SPIStar.h
//  Lection2
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

typedef NS_ENUM(NSInteger, SPIStarType) {
	SPIStarTypeDefault,
	SPIStarTypeBig,
	SPIStarTypeSmall,
};

@interface SPIStar : SPISpaceObject

@property (nonatomic, assign, getter=Weight) NSNumber* weight;
@property (nonatomic, assign) SPIStarType starType;
@property (nonatomic, strong, readonly) NSString *starName;


- (instancetype)initWithType:(SPIStarType)starType starName:(NSString *)starName;
- (NSString *)title;


@end
