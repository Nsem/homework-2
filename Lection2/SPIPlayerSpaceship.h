//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
	SPIPlayerSpaceshipCommandFlyToNextStarSystem,
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;
@property (nonatomic, strong) NSArray *starSystems;  //array of systems (obj)
@property (nonatomic, strong) SPIStarSystem *starSystem;
@property (nonatomic, strong) NSArray *systemsName; // obj names
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;

- (void) loadStarSystem:(NSArray* ) starSystem systemsName:(NSArray *) systemsName;


- (instancetype)initWithName:(NSString *)name;

- (NSString *)availableCommands;

- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
